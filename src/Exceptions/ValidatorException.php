<?php
namespace Sinta\LaravelValidator\Exceptions;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\MessageBag;

class ValidatorException extends \Exception implements Jsonable, Arrayable
{
    protected $messageBag;

    public function __construct(MessageBag $messageBag)
    {
        $this->messageBag = $messageBag;
    }


    public function getMessageBag()
    {
        return $this->messageBag;
    }

    public function toArray()
    {
        return [
            'error'=>'validation_exception',
            'error_description'=>$this->getMessageBag()
        ];
    }


    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }
}